<?php

/********* Actions working stuff  (if a button in index.php is clicked)  *******/
/*
    URL Formats : 
    ?action=deletePost&id=6
    ?action=createPost   post's params will be set by the form when it will be submitted
*/
if (isset($_GET['action'])) {

    switch ($_GET['action']) {
        case 'deletePost':
            $database->deletePost($_GET['id']);
            $url = 'http://localhost/Blog_Poo/';
            header( "Location: $url" );
            break;
        case 'updatePost':
            var_dump($_GET);
            $database->updatePost($_POST['id'], $_POST['title'], $_POST['content']);
            echo '<div class="alert alert-success" role="alert">
                                update !
                            </div>';
            break;
        case 'createPost':
            if(isset($_POST['titre']) && $_POST['titre'] !== "") {
                $result = $database->insertPost($_POST['titre'], $_POST['description']); 
                $url = 'http://localhost/Blog_Poo/';
                header( "Location: $url" );
            }
            break;
        case 'seePost':
            $database->getPost($_POST['id']);
            break;
        case 'validateCode':
            echo '<div class="alert alert-success" role="alert">
                                click for controlScript ! 
                            </div>';
            // see in index.php for ValidationBot side !! (not for students)
            break;
        default:
            echo '<div class="alert alert-warning" role="alert">
                    Action do not exist !!
                </div>';
            break;
    }
}